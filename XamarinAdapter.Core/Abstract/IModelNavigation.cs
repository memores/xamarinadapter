﻿using System.Threading.Tasks;


namespace MvvmBase.XamarinAdapter
{
    public interface IModelNavigation<TView> where TView : class 
    {
        /// <summary>
        /// Asynchronously removes the most recent <see cref="T:Xamarin.Forms.Page"/> from the navigation stack.
        /// </summary>
        /// 
        /// <returns>
        /// The <see cref="T:Xamarin.Forms.Page"/> that had been at the top of the navigation stack.
        /// </returns>
        /// 
        /// <remarks>
        /// To be added.
        /// </remarks>
        Task<TView> PopAsync();
        

        /// <param name="view">The <see cref="T:Xamarin.Forms.Page"/> to be pushed on top of the navigation stack.</param>
        /// <summary>
        /// Asynchronously adds a <see cref="T:Xamarin.Forms.Page"/> to the top of the navigation stack.
        /// </summary>
        /// 
        /// <returns>
        /// A task representing the asynchronous dismiss operation.
        /// </returns>
        /// 
        /// <remarks>
        /// 
        /// <para>
        /// The following example shows <see cref="M:Xamarin.Forms.INavigation.Push"/> and <see cref="M:Xamarin.Forms.INavigation.Pop"/> usage:
        /// 
        /// </para>
        /// 
        /// <example>
        /// 
        /// <code lang="C#">
        /// <![CDATA[
        /// var newPage = new ContentPage ();
        /// await Navigation.PushAsync (newPage);
        /// Debug.WriteLine ("the new page is now showing");
        /// var poppedPage = await Navigation.PopAsync ();
        /// Debug.WriteLine ("the new page is dismissed");
        /// Debug.WriteLine (Object.ReferenceEquals (newPage, poppedPage)); //prints "true"
        /// 							]]>
        /// </code>
        /// 
        /// </example>
        /// 
        /// </remarks>
        Task PushAsync(TView view);
    }
}