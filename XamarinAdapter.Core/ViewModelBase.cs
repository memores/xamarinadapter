﻿using System.ComponentModel;
using PSMvvmBase.Annotations;


namespace MvvmBase.XamarinAdapter
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged(string propertyName) {
            var d = PropertyChanged;
            if (d != null)
                d(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
