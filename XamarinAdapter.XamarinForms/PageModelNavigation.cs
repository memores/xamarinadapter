using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace MvvmBase.XamarinAdapter
{
    public class PageModelNavigation : IModelNavigation<Page>, INavigation
    {
        private readonly INavigation _navigation;


        public PageModelNavigation(INavigation navigation) {
            _navigation = navigation;
        }


        public void RemovePage(Page page) {
            _navigation.RemovePage(page);
        }


        public void InsertPageBefore(Page page, Page before) {
            _navigation.InsertPageBefore(page, before);
        }
        

        public Task PushAsync(Page page) {
            return _navigation.PushAsync(page);
        }


        public Task<Page> PopAsync() {
            return _navigation.PopAsync();
        }


        public Task PopToRootAsync() {
            return _navigation.PopToRootAsync();
        }


        public Task PushModalAsync(Page page) {
            return _navigation.PushModalAsync(page);
        }


        public Task<Page> PopModalAsync() {
            return _navigation.PopModalAsync();
        }


        public Task PushAsync(Page page, bool animated) {
            return _navigation.PushAsync(page, animated);
        }


        public Task<Page> PopAsync(bool animated) {
            return _navigation.PopAsync(animated);
        }


        public Task PopToRootAsync(bool animated) {
            return _navigation.PopToRootAsync(animated);
        }


        public Task PushModalAsync(Page page, bool animated) {
            return _navigation.PushModalAsync(page, animated);
        }


        public Task<Page> PopModalAsync(bool animated) {
            return _navigation.PopModalAsync(animated);
        }


        public IReadOnlyList<Page> NavigationStack {
            get { return _navigation.NavigationStack; }
        }


        public IReadOnlyList<Page> ModalStack {
            get { return _navigation.ModalStack; }
        }
    }
}