using System.Windows.Input;


namespace MvvmBase.XamarinAdapter.Controls.Abstract
{
    public interface IClickableItemsControl {
        ICommand ItemClickCommand { get; set; }
    }
}