﻿using Xamarin.Forms;


namespace MvvmBase.XamarinAdapter
{
    public abstract class NavigatedPageModelBase : ViewModelBase
    {
        public INavigation Navigation { get; set; }


        protected NavigatedPageModelBase(INavigation navigation) {
            Navigation = navigation;
        }
    }
}
