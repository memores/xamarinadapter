﻿using System.Windows.Input;
using MvvmBase.XamarinAdapter.Controls.Abstract;
using Xamarin.Forms;


namespace MvvmBase.XamarinAdapter.Controls
{
    public class AdaptedListView : ListView, IClickableItemsControl
    {
        public static BindableProperty ItemClickCommandProperty = BindableProperty.Create<AdaptedListView, ICommand>(x => x.ItemClickCommand, null);

        public ICommand ItemClickCommand {
            get { return (ICommand)GetValue(ItemClickCommandProperty); }
            set { SetValue(ItemClickCommandProperty, value);}
        }

        
        public AdaptedListView() {
            ItemTapped += OnItemTapped;
        }


        void OnItemTapped(object sender, ItemTappedEventArgs args) { 
            if (args.Item != null && ItemClickCommand != null && ItemClickCommand.CanExecute(args)) {
                ItemClickCommand.Execute(args.Item);
                SelectedItem = null;
            }
        }

    }


}
