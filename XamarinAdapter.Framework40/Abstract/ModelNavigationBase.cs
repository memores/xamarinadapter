﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Remoting.Channels;
using System.Threading.Tasks;
using PSMvvmBase.Annotations;


namespace MvvmBase.XamarinAdapter
{
    public abstract class ModelNavigationBase<TView> : IModelNavigation<TView>, INotifyPropertyChanged
        where TView : class
    {
        private readonly ConcurrentStack<TView> _navigationStack = new ConcurrentStack<TView>();
        private TView _topPage;
        

        public TView TopPage {
            get { return _topPage; }
            set {
                _topPage = value;
                OnPropertyChanged("TopPage");
            }
        }
        
        public Task<TView> PopAsync() {
            return Task.Factory.StartNew(() => {
                TView view;
                if (_navigationStack.TryPop(out view)) {
                    return TopPage = _navigationStack.TryPeek(out view) ? view : null;
                }
                return null;
            });
        }


        public Task PushAsync(TView view) {
            return Task.Factory.StartNew(() => {
                _navigationStack.Push(view);
                return TopPage = view;
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}