﻿using System.Windows.Controls;


namespace MvvmBase.XamarinAdapter
{
    public abstract class ControlPage: Page, IControlPage
    {
        public object BindingContext
        {
            get { return DataContext; }
            set { DataContext = value; }
        }
        
        public INavigation Navigation {
            get { return DataContext as INavigation;}
        }
    }
}
