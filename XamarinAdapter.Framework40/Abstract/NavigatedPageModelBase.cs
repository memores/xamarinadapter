using System.Collections.Generic;
using System.Windows.Controls;


namespace MvvmBase.XamarinAdapter
{
    public abstract class NavigatedPageModelBase : ViewModelBase
    {
        public INavigation Navigation { get; private set; }


        protected NavigatedPageModelBase(INavigation navigation) {
            Navigation = navigation;
        }
    }
}