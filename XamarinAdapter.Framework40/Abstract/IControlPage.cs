
namespace MvvmBase.XamarinAdapter
{
    public interface IControlPage
    {
        object BindingContext { get; set; }

        INavigation Navigation { get; }
    }


}