using System;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Commands;


namespace MvvmBase.XamarinAdapter
{
    public class AsyncCommand : DelegateCommand
    {
        public AsyncCommand(Action executeMethod) : base(() => Task.Factory.StartNew(executeMethod)) { }
        public AsyncCommand(Action executeMethod, Func<bool> canExecuteMethod) : base(() => Task.Factory.StartNew(executeMethod), canExecuteMethod) {}
    }
}