using System;
using Microsoft.Practices.Prism.Commands;


namespace MvvmBase.XamarinAdapter
{
    public class Command : DelegateCommand
    {
        public Command(Action executeMethod) : base(executeMethod) {}
        public Command(Action executeMethod, Func<bool> canExecuteMethod) : base(executeMethod, canExecuteMethod) {}
    }


    public class Command<T> : DelegateCommand<T>
    {
        public Command(Action<T> executeMethod) : base(executeMethod) {}
        public Command(Action<T> executeMethod, Func<T, bool> canExecuteMethod) : base(executeMethod, canExecuteMethod) {}
    }
}