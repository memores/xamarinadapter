﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;


namespace MvvmBase.XamarinAdapter.Controls.Abstract
{
    public interface IClickableItemsControl
    {
        ICommand ItemClickCommand { get; set; }
    }
}
